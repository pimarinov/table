# Table

[![Build Status](https://scrutinizer-ci.com/b/webdivane/table/badges/build.png?b=master)](https://scrutinizer-ci.com/b/webdivane/table/build-status/master)
[![Scrutinizer Code Quality](https://scrutinizer-ci.com/b/webdivane/table/badges/quality-score.png?b=master)](https://scrutinizer-ci.com/b/webdivane/table/?branch=master)
[![MIT license](https://img.shields.io/badge/License-MIT-blue.svg)](https://bitbucket.org/webdivane/table/src/master/LICENSE.md)

* [BitBucket URL](https://bitbucket.org/webdivane/table)
* Version 1.1.2-beta
* Public since: 2016-06-25

## Installation

You can install the package via composer:

```bash
composer require webdivane/table
```

## Usage

```php
    table::create("users", "id", "desc", 50);

        table::$cols[] = ["ID", "id", ["width"=>"50px"]];
        table::$cols[] = ["Name", "name"];
        table::$cols[] = ["Email", "email"];
        table::$cols[] = ["test","test",["sort"=>false]];
        table::$cols[] = ["Created", "created_at_formatted", ["sort"=>"created_at","width"=>"130px"]];
        table::$cols[] = ["Updated", "updated_at_formatted", ["sort"=>"updated_at","width"=>"130px"]];
        table::$cols[] = ["...", "lnks", ["class"=>"links","sort"=>false]];

    table::execute('SELECT * FROM users');

        foreach(table::$data as &$cells){
            $cells = get_object_vars($cells); //object -> array

            if(table::$export === false){
                $cells['id'] = sprintf('<a href="/users/%1$d" title="View">%1$d</a>', $cells['id']);
                $cells['email'] = sprintf('<a href="mailto:%1$s" title="Send email">%1$s</a>', $cells['email']);
            }
        }

        table::$attributes["table"]["class"] = "table-striped table-sm table-hover table-bordered";

    table::load();
```

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.