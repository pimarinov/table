<?php

class Tfoot
{
    use TraitConfig;
    use TraitHelper;
    use TraitPaging;
    use TraitQuery;
    /** @param string class name, where $helper::prepare() is expected */
    public static $helperClass;
    /** @param closure to MySql select function (example: db::select()) */
    public static $select;

    protected static function jsonTfoot()
    {
        if (count(self::$data) > 0) {
            $vars = [];
            self::showingMessageVars($vars);
            if (self::$exportActive === true) {
                $url = strtok(self::$t['slug'], '?&').'.json?table='.
                    self::$t['items'].'&export=';
                $vars['export']['url'] = $url;
                $vars['export']['types'] = self::config('SAVES');
            }
            $vars['errors'] = self::error();
            self::paging($vars);
            $html = self::view('views/table.footer.html', $vars);

            return [[[$html, ['colspans' => count(self::$cols)]]]];
        }

        return [];
    }

    private static function showingMessageVars(&$vars)
    {
        $pageNo = self::$t['page'];
        if ($pageNo === 1 && count(self::$data) < self::$t['paging']) {
            //Skips total count query
            self::$t['rows'] = count(self::$data);
        } else {
            $query = 'SELECT COUNT(*) FROM ('.self::$t['qAll'].') AS dt';
            self::$t['rows'] = self::select($query);
        }
        self::$t['pages'] = ceil(self::$t['rows'] / self::$t['paging']);

        $vars['from'] = ($pageNo - 1) * self::$t['paging'] + 1;
        $vars['upto'] = ($pageNo * self::$t['paging'] < self::$t['rows']) ?
            $pageNo * self::$t['paging'] :
            self::$t['rows'];

        $vars['total'] = self::$t['rows'];
        $vars['items'] = self::$t['items'];
    }

    protected static function rowsTfoot()
    {
        $trs = '';
        if (self::$t['rows'] > 0) {
            $ftr = self::jsonTfoot()[0][0];
            $trs .= '<tr><td'.self::attributes($ftr[1]).'>'
                .$ftr[0].'</td></tr>';
        } elseif (count(self::$errors) > 0) {
            $trs .= '<tr><td colspan="'.count(self::$cols).'">'.
                self::error().'</td></tr>';
        }

        return $trs;
    }

    public static function select(string $expression, array $bindings = [])
    {
        if (is_object(static::$select) && (static::$select instanceof Closure)) {
            $select = static::$select;
            $res = $select($expression, $bindings);
            //if result is single cell value ($res[0]->value), return the value
            return (count($res) === 1 && count((array) $res[0]) === 1) ?
                reset($res[0]) :
                $res;
        } else {
            throw new Exception('ERROR: table::$select is not a closure. ');
        }
    }
}
