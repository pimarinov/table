var table_singleton_methods = {
    helper: window.table_helper,
    ColumnHover: function(tableContainer, index){
        var rows = document.getElementById(tableContainer).rows;
        var upto = rows.length - 1;
        if(typeof index === "undefined"){
            HoverRelease(rows, upto);
        }else{
            for(var i = 0; i < upto; i++){
                rows[i].cells[index].setAttribute("lang", "col-hover");
            }
        }
        function HoverRelease(rows, upto){
            for(var i = 0; i < upto; i++){
                for(var j = 0; j < rows[i].cells.length; j++){
                    if(rows[i].cells[j].lang){
                        rows[i].cells[j].removeAttribute("lang");
                    }
                }
            }
        };
    },
    Draw: {
        Section: function(tableContainer, dt, tSection){
            var section = tSection === "tfoot" ? "tfoot" : "tbody";
            var tSec = document.getElementById(tableContainer)
                        .getElementsByTagName(section)[0];
            tSec.innerHTML="";
            for(var i = 0; i < dt.length; i++){
                tSec.appendChild( Row(dt[i]) );
            }
            if(section === "tfoot"){
                this.helper.ProcessPaginationLinks(tSec);
            }
            function Row(row){
                var tRow = document.createElement("tr");
                for(var i = 0; i < row.length; i++){
                    tRow.appendChild(Cell(row[i]));
                }
                return tRow;
            }
            function Cell(cell){
                var tCell = document.createElement("td");
                if(typeof cell === "string" ||
                    typeof cell === "number"
                ){
                    tCell.innerHTML = cell;
                }else if(typeof cell === "object"){
                    tCell.innerHTML = cell[0];
                    for(var attr in cell[1]){
                        tCell.setAttribute(attr, cell[1][attr]);
                    }
                }
                return tCell;
            }
        },
        Run: function(tableContainer, d, instance){
            this.Section.call(instance, tableContainer, d.body);
            this.Section.call(instance, tableContainer, d.footer, "tfoot");
            if(instance.rq !== null){
                var hover = document.getElementById(instance.rq.tableId)
                        .getElementsByTagName("th")[instance.rq.colNo].lang;
                if(hover){
                    instance.ColumnHover(tableContainer, instance.rq.colNo);
                }
            }
        }
    },
    Export: function(lnk, eType){
        var tableId = this.helper.GetParent(lnk, "table").getAttribute("id");
        var rq = this.helper.BuildRequest.Run.call(this, tableId);
        rq.exportType = ["CSV", "Excel"].indexOf(eType) >= 0 ?
                eType :
                "csv";
        window.open(this.helper.RequestToUrl(rq));
        return false;
    },
    Filter: {
        Run: function(field){
            var tableId = this.Filter.GetTableId(field);
            if(tableId !== null){
                /*var exRq = this.rq;*/
                var rq = this.helper.BuildRequest.Run.call(this, tableId);
                this.rq = rq;
                this.LoadData.Run(this);
                //@todo exRq match not work
                /*if(exRq === null
                        || typeof this.rq.filer === "undefined"
                        || this.rq.filter !== exRq.filter
                        || this.rq.filterBy !== exRq.filterBy
                        ){
                    this.LoadData.Run(this);
                }*/
            }
        },
        GetTableId: function(field){
            var f = field.tagName.toLowerCase() !== "select" ?
                    field :
                    field.parentNode.parentNode.getElementsByTagName("input")[0];
            return '' === f.value ? null : f.getAttribute("data-table-id");
        }
    },
    GoPage: {
        Run: function(lnk){
            var tableId = this.helper.GetParent(lnk, "table").getAttribute("id");
            var rq = this.helper.BuildRequest.Run.call(this, tableId);
            rq.pageNo = this.GoPage.GetNo(lnk, rq.tableId);
            this.rq = rq;
            this.LoadData.Run(this);
            return false;
        },
        GetNo: function(lnk, tableId){
            //check & serve pagination jump links
            var jumpDir = lnk.innerHTML.trim().substr(0, 1);
            if(jumpDir === "+" || jumpDir === "-"){
                var current = document.getElementById(tableId)
                        .querySelector("tfoot .paging .a").innerHTML;
                var jump = lnk.innerHTML.replace("K", "000")
                        .replace("M", "000000000");
                var jumpPage = (parseInt(current) + parseInt(jump));
                lnk.parentNode.setAttribute("data-page", jumpPage);
                lnk.style.transform = "none";
            }
            return lnk.parentNode.hasAttribute("data-page") ?
                    lnk.parentNode.getAttribute("data-page") :
                    lnk.innerHTML;
        }
    },
    Init: function(tableId){
        var tContainer = document.getElementById(tableId);
        if(!this.helper.IePrior(9)){
            SetColumnsHover(tContainer, tableId);
        }
        var tfoot = tContainer.getElementsByTagName("tfoot")[0];
        this.helper.ProcessPaginationLinks(tfoot);

        function SetColumnsHover(tContainer, tableId){
            var tHcells = tContainer.rows[0].cells;
            for(var i = 0; i < tHcells.length; i++){
                if(tHcells[i].firstChild.tagName === "A"){
                    tHcells[i].firstChild.setAttribute("onmouseover",
                            "table.ColumnHover('" + tableId + "'," + i + ");");
                    tHcells[i].firstChild.setAttribute("onmouseout",
                            "table.ColumnHover('" + tableId + "');");
                }
            }
        }
    },
    LoadData: {
        tail: null,
        Run: function(instance){
            if(this.tail !== null){
                this.tail.abort();
            }
            SetVisability(instance.rq.tableId, false);
            var inst = instance;
            var xmlhttp = window.XMLHttpRequest ?
                    new XMLHttpRequest() : //IE7+, Firefox, Chrome, Opera, Safari
                    new window.ActiveXObject("Microsoft.XMLHTTP");//IE6, IE5
            xmlhttp.onreadystatechange = function(){
                if(xmlhttp.readyState === 4 && xmlhttp.status === 200){
                    var d = JSON.parse(xmlhttp.responseText);
                    inst.Draw.Run(inst.rq.tableId, d, inst);
                    SetVisability(inst.rq.tableId, true);
                    window.table.LoadEndCalback(inst.rq.tableId);
                }
            };
            xmlhttp.open("GET", instance.helper.RequestToUrl(inst.rq), true);
            xmlhttp.send();
            this.tail = xmlhttp; //put at tail to can abort later previous request
            
            function SetVisability(tableContainer, flag){
                var tbl = document.getElementById(tableContainer);
                if(flag === true){
                    tbl.style.filter = "none";
                    tbl.style.opacity = "1";
                    tbl.style.cursor = "auto";
                }else if(flag === false){
                    tbl.style.filter = "blur(1px)";
                    tbl.style.opacity = "0.8";
                    tbl.style.cursor = "wait";
                }else{
                    console.error("table error in the flag value");
                }
            }
        }
    },
    ReloadData: function(tableId){
        var rq = this.helper.BuildRequest.Run.call(this, tableId);
        this.rq = rq;
        this.LoadData.Run(this);
    },
    Sort: function(colNo, lnk){
        var tableId = this.helper.GetParent(lnk, "table").getAttribute("id");
        var rq = this.helper.BuildRequest.Run.call(this, tableId);
        if(Math.round(colNo) === rq.colNo){
            rq.colOrd = (rq.colOrd === "asc" ? "desc" : "asc");
        }else{
            rq.colNo = Math.round(colNo);
            rq.colOrd = "asc";
        }
        this.rq = rq;
        this.LoadData.Run(this);
        ClearAndSetNewSortArrow(rq);

        function ClearAndSetNewSortArrow(rq){
            var table = document.getElementById(rq.tableId);
            var headSpans = table.getElementsByTagName("thead")[0]
                    .getElementsByTagName("span");
            var length = headSpans.length;
            for(var i = 0; i < length; i++){
                headSpans[i].innerHTML = "";
            }
            lnk.getElementsByTagName("span")[0].innerHTML = 
                    rq.colOrd === 'desc' ? 
                    table.getAttribute('data-sort-d') : 
                    table.getAttribute('data-sort-a');
        }
    }
};

//https://addyosmani.com/resources/essentialjsdesignpatterns/book/#singletonpatternjavascript
var table_singleton = (function(){
    // Instance stores a reference to the Singleton
    var instance;
    function initInstance(){
        // Singleton
        // Private methods and variables

        var static = table_singleton_methods;

        var ColumnHover = static.ColumnHover;
        var Export = static.Export.bind(static);
        var Filter = static.Filter.Run.bind(static);
        var GoPage = static.GoPage.Run.bind(static);
        var Init = static.Init.bind(static);
        var ReloadData = static.ReloadData.bind(static);
        var Sort = static.Sort.bind(static);

        return {
            init: Init,
            ColumnHover: ColumnHover,
            Export: Export,
            Filter: Filter,
            GoPage: GoPage,
            LoadEndCalback: function(){}, /*Allows override: function(tableId){if(tableId){...}}*/
            ReloadData: ReloadData,
            Sort: Sort
        };
    }
    return {
        //Get the Singleton instance if one exists, or create one if it doesn't
        getInstance: function(){
            if(!instance){
                instance = initInstance();
            }
            return instance;
        }
    };
})();
var table = table_singleton.getInstance();