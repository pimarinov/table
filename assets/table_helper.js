var table_helper = {
    BuildRequest: {
        Run: function(tableId){
            var rq = {tableId: tableId};
            var parent = this.helper.BuildRequest;
            parent.sort(rq);
            parent.filter(rq);
            return rq;
        },
        sort: function(rq){
            var table = document.getElementById(rq.tableId);
            var thTags = table.getElementsByTagName("thead")[0]
                        .getElementsByTagName("th");
            var length = thTags.length;
            for(var i = 0; i < length; i++){
                var link = thTags[i].getElementsByTagName("a")[0];
                if(link){
                    var span = link.getElementsByTagName("span")[0];
                    if(span && sortBySpan(span, i, table)){
                        break;
                    }
                }
            }

            function sortBySpan(span, i, table){
                var order = span.innerHTML;
                if(order.length === 1){
                    rq.colNo = i;
                    var dSymbol = table.getAttribute('data-sort-d');
                    rq.colOrd = order === dSymbol ? "desc" : "asc";
                }
                return rq.colNo === i;
            }
        },
        filter: function(rq){
            var r = getFilterFieldsByTableID(rq.tableId);
            if(r.filter !== null){
                rq.filter = r.filter;
            }
            if(r.filterBy !== null){
                rq.filterBy = r.filterBy;
            }

            function getFilterFieldsByTableID(tableID){
                var fields = {filterBy: null, filter: null};
                var filterDiv = getFilterDivByTableIDOrNull(tableID);
                if(filterDiv !== null){
                    setFilterBy(fields, filterDiv);
                    setFilterValue(fields, filterDiv);
                }
                return fields;

                function getFilterDivByTableIDOrNull(tableID){
                    if(document.getElementById(tableID).parentNode
                            .getElementsByTagName("div").length > 0
                            ){
                        var containerDivs = document.getElementById(tableID)
                                .parentNode.getElementsByTagName("div");
                        for(var i = 0; i < containerDivs.length; i++){
                            if(containerDivs[i].getAttribute("class") === "filter"){
                                return containerDivs[i];
                            }
                        }

                    }
                    return null;
                }

                function setFilterBy(fields, filterDiv){
                    var select = filterDiv.getElementsByTagName("select")[0];
                    if(select &&
                            select.options[select.selectedIndex].value !== "all"
                            ){
                        fields.filterBy = select.options[select.selectedIndex].value;
                    }
                }
                function setFilterValue(fields, filterDiv){
                    var textObj = filterDiv.getElementsByTagName("input")[0];
                    if(textObj && textObj.value && textObj.value.length !== 0){
                        fields.filter = encodeURIComponent(textObj.value.trim());
                    }
                }
            }
        }
    },
    IePrior: function(v){
        var rv = false;
        if(window.navigator.appName === 'Microsoft Internet Explorer'){
            var ua = window.navigator.userAgent;
            var re = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
            if(re.exec(ua) !== null){
                rv = parseFloat(RegExp.$1);
            }
            rv = rv < v ? true : false;
        }
        return rv;
    },
    GetParent: function(obj, objType){
        while(obj && obj.tagName !== objType.toUpperCase()){
            obj = obj.parentNode;
        }
        return obj;
    },
    ProcessPaginationLinks: function(tfoot){
        var pLinks = tfoot.querySelectorAll(".paging a");
        if(pLinks.length > 0){
            for(var j = 0; j < pLinks.length; j++){
                pLinks[j].setAttribute("href", "javascript:void(0);");
                pLinks[j].setAttribute("onclick", "return table.GoPage(this);");
            }
        }
    },
    RequestToUrl: function(rq){
        var url = location.pathname + ".json" + location.search;
        var getUrlVarName = {
            colNo: "col", colOrd: "ord", filter: "filter",
            filterBy: "filter-by", pageNo: "pg", exportType: "export",
            tableId: "table-id"
        };
        var flagFirst = location.search.length < 1 ? true : false;
        for(var r in rq){
            var clue = flagFirst === true ? "?" : "&";
            url += clue + getUrlVarName[r] + "=" + rq[r];
            flagFirst = false;
        }
        return url;
    }
};
