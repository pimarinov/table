<?php

use PHPUnit\Framework\TestCase;

class TableTest extends TestCase
{
    /**
     * @covers Table::prepare
     * @covers Table::confirmValue
     */
    public function testprepare()
    {
        $_SERVER['REQUEST_URI'] = 'test.json';

        //Table::prepare(true);

        Table::prepare();
        $this->assertSame(Table::confirmValue('t.slug', 'test.json'), true);
        $this->assertSame(Table::$pageExt, 'json');

        $_SERVER['REQUEST_URI'] = null;

        Table::prepare();
        $this->assertSame(Table::confirmValue('t.slug', ''), true);
        $this->assertSame(Table::$pageExt, '');
    }

    /**
     * @covers Table::prepare
     * @covers Table::create
     * @covers Table::error
     * @covers Table::config
     */
    public function testcreate()
    {
        $_SERVER['REQUEST_URI'] = 'test/users';

        Table::prepare();

        $this->assertSame(Table::confirmValue('t.page', 1), true);
        //$this->assertSame(Table::confirmValue('t.tables', []), true);

        $items = 'users-01';
        $orderBy = 'id';

        Table::create($items, $orderBy, 'a');
        $this->assertSame(Table::confirmValue('t.items', $items), true);
        $this->assertSame(Table::confirmValue('t.order.col', $orderBy), true);
        $err = Table::confirmValue('errors.0', 'Invalid orderDir (Asc/Desc): a');
        $this->assertSame($err, true);

        Table::create($items, $orderBy, 'asc', 2);
        $this->assertSame(Table::confirmValue('t.items', $items), true);
        $this->assertSame(Table::confirmValue('t.order.col', $orderBy), true);
        $this->assertSame(Table::confirmValue('t.order.dir', 'asc'), true);
        $err1 = Table::confirmValue('errors.last', 'Invalid paging (<10): 2');
        $this->assertSame($err1, true);
    }

    /**
     * @covers Table::execute
     * @covers Table::request
     * @covers Table::filter
     * @covers Table::filterByAll
     * @covers Table::orderCol
     * @covers Table::orderDir
     * @covers Table::setExport
     * @covers Table::page
     */
    public function testexecute()
    {
        $_SERVER['REQUEST_URI'] = 'test/users';

        Table::prepare();

        $items = 'users';
        $orderBy = 'id';
        Table::create($items, $orderBy);

        $query = 'SELECT `id` FROM `table`';
        Table::execute($query);
        $this->assertSame(Table::$export, false);
    }

    /**
     * @covers Table::load
     * @covers Thead::load
     * @covers Thead::filterValues
     * @covers Thead::tagAttributes
     * @covers Thead::ths
     * @covers Thead::thAttributes
     * @covers Thead::thTag
     * @covers Tbody::rowsTbody
     * @covers Table::config
     * @covers Table::valid
     */
    public function testload()
    {
        $_SERVER['REQUEST_URI'] = 'test/articles';

        Table::prepare();

        $items = 'articles';
        $orderBy = 'id';
        Table::create($items, $orderBy);
        Table::$cols = [['id']];
        $query = 'SELECT `id` FROM `articles`';
        Table::execute($query);

        Table::load();
        $regex = '/(.*)$/m';
        $this->expectOutputRegex($regex);
    }

    /**
     * @covers Table::load
     * @covers Tbody::jsonTbody
     * @covers Tfoot::jsonTfoot
     */
    public function testloadjson()
    {
        $_SERVER['REQUEST_URI'] = 'test/items.json';
        $_GET['table-id'] = 'items-table';

        Table::prepare();

        $items = 'items';
        $orderBy = 'id';
        Table::create($items, $orderBy);
        Table::$cols = [['id']];
        $query = 'SELECT `id` FROM `items`';
        Table::execute($query);

        $body = [[['No results found.', ['colspan' => 1, 'class' => 'no-results']]]];
        $jsonArr = ['body' => $body, 'footer'=> []];

        Table::load();

        $this->expectOutputString(json_encode($jsonArr));
    }

    /**
     * @covers Table::load
     * @covers Tbody::export
     * @covers Tbody::exportColumnsAndHeader
     * @covers Tbody::exportFile
     * @covers Tbody::exportFileName
     */
    public function testloadexport()
    {
        $_SERVER['REQUEST_URI'] = 'test/products.json';
        $_GET['table-id'] = 'products-table';
        $_GET['export'] = 'CSV';

        Table::prepare();

        $items = 'products';
        $orderBy = 'id';
        Table::create($items, $orderBy);
        Table::$cols = [['id', 'ID']];
        $query = 'SELECT `id` FROM `products`';
        Table::execute($query);

        $this->assertTrue(Table::load());
    }

    /**
     * @codeCoverageIgnore
     */
    public static function prepare($classMethod, $setOrCheck = false)
    {
        self::$pageExtension = pathinfo($_SERVER['REQUEST_URI'], PATHINFO_EXTENSION) ?: null;
        $continuePreparation = $setOrCheck === false;
        if ($setOrCheck === true) {
            if (!in_array($classMethod, self::$preparePassed) && empty(self::$pageExtension)) {
                exit('ERROR (in a '.__CLASS__.' call): Please call the '.$classMethod.'(); method before the page headers sent.');
            }
        } elseif (!empty($setOrCheck)) {
            self::$pageExtension = $setOrCheck;
        }
        if (!in_array($classMethod, self::$preparePassed)) {
            self::$preparePassed[] = $classMethod;
        }
        table::$select = function (string $expression, array $bindings = []) {
            return substr($expression, 0, 18) === 'SELECT DATE_FORMAT' ?
                   [['']] : [];
        };

        return $continuePreparation;
    }

    public static $rustart;
    public static $pageExtension;
    public static $preparePassed = [];

    /**
     * @codeCoverageIgnore
     */
    public function setUp(): void
    {
        Table::$helperClass = __CLASS__;
        parent::setUp();
    }
}
